var gulp = require('gulp');

// selitykset plugareille

// connect 					 livereload serveri
// log 						 watchille vaihtoehtoinen nimi, watch nimen käyttö taskin nimenä aiheuttaa errorin
// lint						 tarkistaa javascriptistä virheitä
// url						 avaa lopuksi projektin selaimessa
// sass                      kääntää scss filut css


gulp.task('serve', ['log', 'connect', 'url', 'lint', 'styles', 'scripts', 'browse']);