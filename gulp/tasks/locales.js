var gulp = require('gulp');

var filesToMove = ['./app/locales/**/*.json'];
var filesNewLocation = ['dist'];

gulp.task('moveLocales', function() {
    gulp.src(filesToMove)
        .pipe(gulp.dest('./dist/locales/'));
});
