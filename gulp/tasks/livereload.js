var gulp = require('gulp'),
    connect = require('gulp-connect'),
    historyApiFallback = require('connect-history-api-fallback');

// Livereload serveri

gulp.task('connect', function() {
  connect.server({
    root: 'app',
    livereload: true,

    middleware: function(connect, opt) {
      return [ historyApiFallback ];
    }

  });
});