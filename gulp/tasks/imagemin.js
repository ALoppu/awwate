var gulp = require('gulp'),
	imagemin = require('gulp-imagemin'),
    pngcrush = require('imagemin-pngcrush');

// Huom
//  Vaikka imagemin dokumentaatiossa ei ole mainintaa vaatii projektin pyöritys silti
// pngcrush erillisen asennuksen
// npm install imagemin-pngcrush 

gulp.task('minimg', function () {
    return gulp.src('./app/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('dist/img/'));
});