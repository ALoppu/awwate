var gulp = require('gulp'),
	open = require('gulp-open');


gulp.task('url', function() {
	var options = {
		// livereload portti
		// mikäli vaihtaa lvr portiksi muunkuin 8080
		// tulee muutos tehdä myös tänne
		url: "http://localhost:8080",
		//selain chrome - firefox - safari - opera
		app: "chrome"
	};
	// avattava sivu
	gulp.src("./app/index.html")
	.pipe(open("", options));
});