var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	connect = require('gulp-connect');

gulp.task('lint', function() {
	return gulp.src(['./app/js/**/*'])
		.pipe(connect.reload())
		.pipe(gulp.dest('./dist/js/'))
		.pipe(jshint.reporter('default', {
			verbose: true
		}));
});