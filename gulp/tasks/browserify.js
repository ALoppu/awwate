var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect');

gulp.task('browse', function() {
  // Single point of entry (make sure not to src ALL your files, browserify will figure it out for you)
  return gulp.src(['app/scripts/app.js'])
  .pipe(browserify({
    insertGlobals: true,
    debug: true
  }))
  // Bundle to a single file
  .pipe(concat('bundle.js'))
  // Output it to our dist folder
  .pipe(connect.reload())
  .pipe(gulp.dest('app/js/'))
  .pipe(gulp.dest('dist/js/'));

});