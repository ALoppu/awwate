var gulp = require('gulp'),
	htmlmin = require('gulp-htmlmin'),
	connect = require('gulp-connect');

gulp.task('html', function() {
	return gulp.src(['./app/**/*.html'])
		.pipe(connect.reload())
		.pipe(gulp.dest('./dist/'))
});