var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	connect = require('gulp-connect');

gulp.task('scripts', function() {
	return gulp.src('./app/scripts/app.js')
		.pipe(connect.reload())
		.pipe(concat('bundle.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./app/scripts/bundle/'))
});