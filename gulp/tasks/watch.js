var gulp = require('gulp'),
	watch = require('gulp-watch');

gulp.task('log', function() {
	gulp.watch(['./app/**/**/**/*.html'], ['html']),
	gulp.watch(['./app/sass/**/*.scss'], ['styles']),
	gulp.watch(['./app/scripts/*.js'], ['lint']),
	gulp.watch(['./app/scripts/**/*.js'], ['scripts', 'browse']);
});