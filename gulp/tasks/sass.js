var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    connect = require('gulp-connect'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css');
gulp.task('styles', function() {
    return gulp.src('./app/sass/**/*')
        .pipe(sass({sourcemap: true, sourcemapPath: 'app/sass/**/'}))
        /* Virheen tullen pitää gulpin päällä
            eikä sammuta. */
        .on('error', function(err) {
            console.log(err.message);
        })
        .pipe(connect.reload())
        /* Lisätään vasta lopuksi, samoin CSS ohjataan
           buildiin lopuksi. 

           Olisi myös mahdollista tehdä tiedostosta 2 kopiota
           buildiin & deviin, lisätään tämäkin jossain välissä. */
        /*.pipe(minifyCSS({
            keepBreaks: true
        }))*/
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest('./app/styles/'))
        .pipe(minifyCSS({keepBreaks:false}))
        .pipe(gulp.dest('./dist/styles/'))

        /* Notify aina pohjalle, muuten ilmoituksia tulee 
           liikaa & turhan aikaisin. */
});
