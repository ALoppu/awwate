    $(document).ready(function() {
        $(".toggler").on("click", function() {
            $(".navbar-ex1-collapse").slideToggle("slow", function() {
            });
        });
    });

    var jumboHeight = $('.jumbotron').outerHeight();
    function parallax() {
        var scrolled = $(window).scrollTop();
        $('.bg').css('height', (jumboHeight - scrolled) + 'px');
    }
    $(window).scroll(function(e) {
        parallax();
    });
