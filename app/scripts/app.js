'use strict';

// Controllers
var WelcomeCtrl = require('./controllers/WelcomeCtrl');
var TestCtrl = require('./controllers/TeamList');
var CvCtrl = require('./controllers/CvCtrl');

// AngularMaps

// Directives


// Services


// ------------------------------------------------------------------


var app = angular.module('fnx', ['ngRoute', 'mgcrea.ngStrap'])
    .config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.
            when('/', {
                templateUrl: 'partials/etusivu.html',
                controller: 'WelcomeCtrl'
            }).
            when('/tekijat', {
                templateUrl: 'partials/tekijat.html',
                controller: 'CvCtrl'
            }).
            otherwise({
                redirectTo: '404',
                templateUrl: 'partials/404.html'
            });
            $locationProvider.html5Mode(true);
        }

    ]);
/*    .run(function($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function() {
            if ($rootScope = $location.path() == "/tekijat") {
                console.log("Tekijät");
            } else {
                console.log("Etusivu");
            }
            $rootScope.showSection = $location.path() == "/tekijat";
            $rootScope.showSection = $location.path() !== "/etusivu";
        });

    });*/
// Kaikille funktioille tulee annotaatiot vasta täällä.
app.controller('CvCtrl', ['$scope', CvCtrl]);
app.controller('WelcomeCtrl', ['$scope', WelcomeCtrl]);
app.controller('TestCtrl', ['$scope', '$http', TestCtrl]);

// AngularMaps
