"use strict";

var CvCtrl = function($scope) {
    $scope.terms = [{
        "java": {
            title: "Java-Osaaja",
            content: "Tarjoamme vakituista työsuhdetta, jossa pääset paraatipaikalle kehittämään vaativia ratkaisuja asiakkaillemme. Omaat kokemusta ja näkemystä ohjelmointikehityksestä ja sinulla on hyvä draivi päällä. Olet hyvä tyyppi ja tiimipelaaja!",
            content_sm: "Kommunikoit asiakkaan kanssa sujuvasti ja omaat palavan halun tehdä parhaasi asiakkaan eteen!",
            "vaatimukset": {
                kettera: "ketterä ohjelmistokehitys",
                testaus: "testauslähtöinen ohjelmistokehitys.",
                jatkuva: "jatkuvan integraation sujuvaa käyttöä",
                java: "Java-teknologioiden hyvää tuntemusta",
                selain: "selain-teknologioiden hyvää tuntemusta",
                kieli: "hyvää puhuttua ja kirjoitettua suomen ja englannin kielen osaamista"
            }
        },
        "dotnet": {
            title: ".NET-Osaaja",
            content: "Haemme vakituiseen työsuhteeseen .NET-osaajaa kehittämään asiakkaillemme vaativia ratkaisuja. Olet joko kokenut ammattilainen tai tulevaisuuden talentti. Haluat kehittyä lisää ja jakaa jo oppimaasi tietoa eteenpäin. Olet tiimipelaaja!",
            content_sm: "Työssäsi kommunikoit eri sidosryhmien kanssa ja asiakaspalveluasenteesi on huippuluokkaa! "
        },
        "projekti": {
            title: "Osaaja X",
            content: "Vaikka sopivaa tehtävää ei olisi tällä hetkellä tarjolla, niin juuri sinulle sopiva paikka voi tulla eteen ennemmin kuin myöhemmin. Laitathan meille vapaan hakemuksen tulemaan. Lupaamme katsoa tilanteesi ajan kanssa läpi ja kartoittaa mahdollisuutesi liittyä mukaan joukkoomme.",
            content_sm: " "
        },
        "titles": {
            odotamme: "Odotamme sinulta",
            arvostamme: "Arvostamme",
            tarjoamme: "Mitä me tarjoamme"
        }
    }];

};



module.exports = CvCtrl;
