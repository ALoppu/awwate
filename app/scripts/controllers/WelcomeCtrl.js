"use strict";

var WelcomeCtrl = function($scope) {
    $scope.terms = [{
        "Java": {
            title: "Test 1",
            content: "Content 1"
        },
        "dotNet": {
            title: "Test 2",
            content: "Test 2"
        }
    }]

};


module.exports = WelcomeCtrl;
