"use strict";


var TestCtrl = function($scope, $http) {
    $http({
        method: 'GET',
        url: '../locales/emp/loyees.json'
    }).
    success(function(data, status, headers, config) {
        for (var i = 0; i < data.length; i++) {
            data[i].name = data[i].name.split(' ').join("\n");
        }
        $scope.emps = data;
    }).
    error(function(data, status, headers, config) {
        console.log(status);
    });
};

module.exports = TestCtrl;
