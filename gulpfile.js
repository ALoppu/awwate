/*

=======================================================================================================
1)

Sensijaan että halittaisiin yhtä jättimäistä tiedostoa on kaikki
gulpin plugarit osioitu omiin tiedostoigin gulp/ kansiossa.

Gulp kansion rakenne

gulp/
|
| - tasks/					// kaikki 'perus' plugarit imagemin etc..
| |- browserSync.js
| |- build.js               // Core 
|  ...
|
| - util/					// notify 'ilmoitus' plugarit & virheidenkäsittely
| |- bundleLogger.js
| |- handeErrors.js
|  ...
|

Lisätessä uusia taskeja gulp pitää aina 'alustaa' taskin alkuun
var gulp = require('gulp')

=======================================================================================================
2)

Gulpin käyttö 

gulp serve - dev käytössä - jättää joitakin taskeja käynnistämättä/suorittamatta
mm. kaikki minifioinnit

gulp - 'default', buildaa pelkästään ilman että käynnistää lvr / open etc...

 */

var requireDir = require('require-dir');

requireDir('./gulp/tasks', { recurse: true });