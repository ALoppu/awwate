### Asennus ###

Asennus

npm install & bower install

npm install -g gulp

html5mode

https://github.com/bripkens/connect-history-api-fallback

Vaatii myös tuon urlin siistimiseksi, eli #etusivu > /etusivu.

### Projektin 'avaus' ###

gulp - Suorittaa kaikki defaultit eli buildaa projektin mutta jättää käynnistämättä lvr(livereload) && projektin avauksen(open).

gulp server - Suorittaa projektin mutta jättää joitakin taskeja suorittamatta mm. kuvien minifioinnin, vie liikaa aikaa. Esimerkkinä toimiston kuva koko: 8mb, 'pakattuna' alle 1mb säästöä taisi olla 96% ja aikaa meni +1min.

### gitignore ###

dist
node_modules
app/bower_components

### Ongelmat ###

Issue trackeri käytössä. <<

### Muuta ###

sassissa värien nimeämisen helpottamiseksi käytössä alla oleva värien 'nimeäjä'.
[http://chir.ag/projects/name-that-color/#565C67](CLRS)